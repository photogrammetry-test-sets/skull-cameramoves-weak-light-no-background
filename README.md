![Preview of Images](./Preview.jpg)

## [More Photogrammetry Test Sets](https://github.com/AlansCodeLog/photogrammetry-test-sets)

## License
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>

### Attribution
```
"Photogrammetry Test Set: Skull - Cameramoves - Weak Light - No Background" by alansartlog / CC BY 4.0
```

If using a specific image, append "# [NUMBER]" to the title.

If you can use links, please link:
- The title to the corresponding repo or the main repo (https://github.com/AlansCodeLog/photogrammetry-test-sets).
- alansartlog => "https://alansartlog.com"
- CC BY 4.0 => "https://creativecommons.org/licenses/by/4.0/"

Here's the markdown version:

```
["Photogrammetry Test Set: Skull - Cameramoves - Weak Light - No Background"](https://github.com/AlansCodeLog/photogrammetry-test-sets) by [alansartlog](https://alansartlog.com) / [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
```